#!/usr/bin/env node

const _ = require('lodash');
const cheerio = require('cheerio');
const colors = require('colors/safe');
const commander = require('commander');
const moment = require('moment');
const request = require('request');
const sprintf = require('sprintf-js').sprintf;

const dateFormat = 'DD-MM-YY HH:mm';
const SCHEDULE_URL = 'https://gamesdonequick.com/schedule';

commander
  .version('1.0.0')
  .option('-l, --limit <n>', 'Limit result to a set amount of entries.', parseInt)
  .option('-r, --relative', 'Convert timestamps to human-readable format relative to the current time.')
  .parse(process.argv);

request.get(SCHEDULE_URL, (err, results) => {
  const $ = cheerio.load(results.body);
  let rows = _.chunk($('table#runTable tbody tr'), 2);

  let runs = _.map(rows, (row) => {
    const fields = _.map($(row).find('td'), $);
    return {
      game: fields[1].text(),
      runner: fields[2].text(),
      runTime: fields[4] ? _.trim(fields[4].text()) : '',
      runType: fields[5] ? fields[5].text() : '',
      startTime: moment(fields[0].text()),
    };
  });

  runs = _.filter(runs, (run) => {
    var endTime = moment(run.startTime).add(moment.duration(run.runTime));
    return moment().isBefore(endTime);
  });

  if (commander.limit) {
    runs = _.take(runs, commander.limit);
  }

  runs = _.map(runs, (run) => {
    if (commander.relative) {
      var difference = run.startTime.diff(moment());
      run.startTime = moment.duration(difference).humanize(true);
    } else {
      run.startTime = run.startTime.format(dateFormat);
    }
    return run;
  });

  _.forEach(runs, (run) => {
    let prettyRun = colors.green(run.startTime) + ' - ' + colors.red(run.game);
    if (run.runType) {
      prettyRun += ' (' + colors.magenta(run.runType)+ ')';
    }
    prettyRun += ' - ' + colors.blue(run.runner);
    if (run.runTime) {
      prettyRun += ' - ' + colors.yellow(run.runTime);
    }
    console.log(prettyRun);
  });
})
